
var Page = React.createClass({

    render: function () {
        return(
            <div>
                <WarContainer/>
                <WarDetailAccordion/>

                <div id="warCommentSection">
                    <div id="leftContent">
                        <div id="commentBox1" className="animated zoomInLeft">
                            <CommentBox color={"#28A9E0"}/>
                        </div>
                        <div id="commentedBox1">
                            <CommentedBox color={"#28A9E0"}/>
                        </div>

                    </div>
                    <div id="rightContent">
                        <div id="commentBox2" className="animated zoomInRight">
                            <CommentBox color={"#F15723"}/>
                        </div>
                        <div id="commentedBox2" className="animated bounceInUp">
                            <CommentedBox color={"#F15723"}/>
                        </div>
                    </div>

                </div>
                <br className="clear"/>

                <LoginModal onClick={this.checkLogin}/>

                <div id="loginContainer" className="animated slideInLeft">
                    <LoginDiv/>
                </div>
            </div>

        );
    }
});


var Header = React.createClass({


    render: function () {
        return (
            <div>
                <div className="ui menu">
                    <a className="item " id="logo">
                        <img  src='../images/slice/logo.png'/>
                    </a>
                    <div className="right menu right-menu">
                        <div className="center">

                            <a className="item">

                                <div className="content">
                                    <div className="header middle">
                                        <img className="img-circular" src="../images/slice/suddgest-war.png"/>
                                    </div>
                                    Suggest a war
                                </div>

                            </a>
                            <a className="item ">
                                <div className="content">
                                    <div className="header middle">
                                        <img className="img-circular" src="../images/slice/notification.png"/>
                                    </div>
                                    Notification
                                </div>
                            </a>
                            <a className="item">
                                <div className="content">
                                    <div className="header middle">
                                        <img className="img-circular" src="../images/slice/profile.png"/>
                                    </div>
                                    Manoj Singh
                                </div>
                            </a>
                            <a className="item">

                                <i className="sidebar icon"></i>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

var WarContainer = React.createClass({

     componentDidMount: function () {
        var width = React.findDOMNode(this).clientWidth;
         console.log(width);
     
     },

    render: function () {
        return (
            <div className = "warContainer">
                <LeftWarContainer/>
                <RightWarContainer/>
                <WarTextTime/>
                <WarLogo/>
            </div>
        );

    }
});


var LeftWarContainer = React.createClass({


    getInitialState: function () {

        var scope = {
            topicDetail: {
                topicName: 'Sallu Miya'

            },

            leftCount: {
                width: '70%'
            }
        };

        return scope;
    },
    /*
     componentDidMount: function() {
     $.get(this.props.source, function(result) {
     var lastGist = result[0];
     if (this.isMounted()) {
     this.setState({
     username: lastGist.owner.login,
     lastGistUrl: lastGist.html_url
     });
     }
     }.bind(this));
     },
     handleClick: function(index) {

     this.setState({items: items}, function() {

     }.bind(this));
     },*/
    render: function () {
        return (

            <section className = "leftWarContainer" id="section-left">
                <div className = "TopicTop">
                    <div>
                        <h3>{this.state.topicDetail.topicName}</h3>
                    </div>
                    <div className="label">110Votes</div>

                    <div className="progress-bar" >
                        <div className=" bar negative" style={{"background": "#28A9E0"}}>
                            <span></span>
                        </div>
                        <div className=" bar positive" id="bar1" style={{"background": "#C1C1C1"}}>
                            <span></span>
                        </div>

                    </div>
                    <div className="label">
                        <button className="ui blue button" style={{"backgroundColor": "#28A9E0"}}>Vote Here for side</button>
                    </div>
                </div>
            </section>
        );

    }
});
var RightWarContainer = React.createClass({

    getInitialState: function () {

        var scope = {
            topicDetail: {
                topicName: 'Sahrukh Bhai'

            },

            leftCount: {
                width: '40%'
            }
        };

        return scope;
    },
    /*
     componentDidMount: function() {
     $.get(this.props.source, function(result) {
     var lastGist = result[0];
     if (this.isMounted()) {
     this.setState({
     username: lastGist.owner.login,
     lastGistUrl: lastGist.html_url
     });
     }
     }.bind(this));
     },
     handleClick: function(index) {

     this.setState({items: items}, function() {

     }.bind(this));
     },*/

    render: function () {
        return (
            <section className = "rightWarContainer" id="section-right">
                <div className = "TopicTop">
                    <div>
                        <h3 style={{"textAlign": "right"}}>{this.state.topicDetail.topicName}</h3>
                    </div>
                    <div className="label" style={{"textAlign": "right"}}>110Votes</div>
                    <div className="progress-bar" >
                        <div className="bar positive " id="bar2" style={{"background": "#C1C1C1"}}>
                            <span></span>
                        </div>
                        <div className="bar negative">
                            <span></span>
                        </div>

                    </div>
                    <div className="label">
                        <button className="ui button " style={{
                            float: "right",
                            "backgroundColor": "#F15723"
                        }}>Vote Here for side</button>
                    </div>

                </div>
                <SubmittedBy/>
            </section>
        );

    }
});


var WarTextTime = React.createClass({

    render: function () {
        return (
            <div className="warTextTime">
                <Wartext/>
                <WarTime/>

            </div>
        )
    }

});


var Wartext = React.createClass({

    render: function () {
        return (
            <div>
                <h2>
                    <b>who is better action star</b>
                </h2>
            </div>
        )
    }

});

var WarTime = React.createClass({

    render: function () {
        return (
            <div>
                <div>
                    <h1>
                        <b>VS</b>
                    </h1>
                </div>

                <div id="time">

                </div>
            </div>
        )
    }

});

var WarLogo = React.createClass({

    render: function () {
        return (
            <div id="warLogo">
                <img  src="../images/fight.png" style={{"width":"60px","height":"60px"}}/>
            </div>
        )
    }

});

var SubmittedBy = React.createClass({

    render: function () {
        return (
            <div className="submittedBy">
                <h4>War submitted by :
                    <span>
                        <a>
                            <b>karan Dada</b>
                        </a>
                        |
                    </span>
                    <span>Share war |</span>
                    <a>
                        <i className="facebook square icon"></i>
                    </a>
                    <a>
                        <i className="twitter square icon"></i>
                    </a>
                    <a>
                        <i className="google plus square icon"></i>
                    </a>
                </h4>

            </div>
        );
    }

});

var WarDetailAccordion = React.createClass({

    render: function () {
        return (
            <div className="ui styled accordion">

                <div className="title">
                    <i className="dropdown icon"></i>
                </div>
                <div className="content">
                    <p>Three common ways for a prospective owner to acquire a dog is from pet shops, private owners, or shelters.</p>
                    <p>A pet shop may be the most convenient way to buy a dog. Buying a dog from a private owner allows you to assess
                        the pedigree and upbringing of your dog before choosing to take it home. Lastly, finding your dog from a shelter,
                        helps give a good home to a dog who may not find one so readily.</p>
                    <div>
                        <div className="accordionDivider1">
                            <div className="bar-one bar-con">
                                <div className="bar" data-percent="20"></div>
                            </div>

                            <div className="bar-two bar-con">
                                <div className="bar" data-percent="50"></div>
                            </div>

                            <div className="bar-three bar-con">
                                <div className="bar" data-percent="70"></div>
                            </div>

                        </div>
                        <div className="middleBtn">
                            <button className="ui active button">Zomato Rating!!</button>
                            <button className="ui active button">Foodpanda Orders</button>
                            <button className="ui active button">Jolly Gymkhana</button>
                        </div>
                        <div className="accordionDivider2">
                            <div className="bar-one bar-con">
                                <div className="bar" data-percent="20"></div>
                            </div>

                            <div className="bar-two bar-con">
                                <div className="bar" data-percent="50"></div>
                            </div>

                            <div className="bar-three bar-con">
                                <div className="bar" data-percent="70"></div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }


});

var RatingTopic = React.createClass({

    getInitialState: function () {


    },

    render: function () {
        return (
            <div className="ui tiny progress" data-percent="27">
                <div className="bar" style={{"transition-duration": "300ms", "width": "27%"}}></div>
                <div className="label">Tiny</div>
            </div>
        );
    }
});

var CommentBox = React.createClass({


    getInitialState: function() {
        return { showForm: false };
    },
    showForm: function() {
        this.setState({showForm: true});
    },
    render: function () {

        var btnClass = {
            background: this.props.color
        };

        return (
            <div>
                <LoginModal open={this.state.showForm}>

                </LoginModal>
            <div className="ui card">
                <div className="content" >
                    <div className="right floated meta">
                        14h
                    </div>
                    <img className="ui avatar image" src="../images/profile_1.jpg"/>
                    Maks
                </div>
                <div className="image">
                    <img/>
                </div>

                <div className="content">
                    <div className="ui form">
                        <div className="field">
                            <textarea className="commentTextBox" rows="1" onClick={this.showForm} ></textarea>
                        </div>
                        <div className="inline fields">
                            <div className="field">
                                <button className="ui primary button" id="btnSmash" style={btnClass}>
                                    Smash
                                </button>
                            </div>
                            <div className="field">
                                <div className="ui checkbox">
                                    <input type="checkbox" name="frequency" checked="checked"/>
                                    <label>
                                        <i className="facebook icon"></i>
                                    </label>
                                </div>
                            </div>
                            <div className="field">
                                <div className="ui checkbox">
                                    <input type="checkbox" name="frequency"/>
                                    <label>
                                        <i className="twitter icon"></i>
                                    </label>
                                </div>
                            </div>
                            <div className="field">
                                <div className="ui checkbox">
                                    <input type="checkbox" name="frequency"/>
                                    <label>
                                        <i className="google plus icon"></i>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            </div>
        );
    }

});

var CommentedBox = React.createClass({


    getInitialState: function () {

        return {
            toggle: false,
            isUpVoted :false,
            upVoteBtnText :"UpVote"
        }
    },
    addUpVote : function () {
        var isUpVoted  = this.state.isUpVoted;
        this.setState({isUpVoted:true,upVoteBtnText:"UpVoted"});

    },

    handleReply: function () {

        var toggle = this.state.toggle;
        console.log(toggle);

        this.setState({toggle: !toggle})
    },
    render: function () {
        var toggle = this.state.toggle;
        var hrStyle = {
            border: "2px thick",
            borderColor: this.props.color

        };
        var bgColor = {
            color: this.props.color
        };
        return (
            <div>
                <div className="ui card">
                    <div className="content">
                        <div className="right floated meta">
                            14h
                        </div>
                        <img className="ui avatar image" src="../images/profile_1.jpg"/>
                        Maks
                    </div>
                    <div className="image">
                        <img/>
                    </div>

                    <div className="text">
                        When using classSet(), pass an object with keys of the CSS class names you might or might not need. Truthy values will result in the key being a part of the resulting string.
                    </div>
                    <hr style={hrStyle}/>
                    <div className="action">
                        <a className="reply1" style={bgColor} onClick={this.addUpVote}>{this.state.upVoteBtnText} | 25</a>
                        <a className="reply" style={bgColor} onClick={this.handleReply}>Reply</a>

                            <div className="ui dropdown link item" style={bgColor} >
                                Share
                                <div className="menu" >
                                    <div className="item" >
                                        <a href="facebook.com/warcircles" style={bgColor}>
                                        facebook
                                        </a>
                                    </div>
                                    <div className="item">
                                        <a href="twitter.com/warcircle" style={bgColor}>twitter</a></div>
                                </div>
                        </div>

                        <a className="hide" style={bgColor}>Comments</a>


                    </div>
            { this.state.toggle ? <ReplyContainer toggle={toggle} color={bgColor.color} handleReply={this.handleReply} /> : null}


                </div>

            </div>
        );
    }

});


var LoginDiv = React.createClass({

    render: function () {
        return (
            <div className="loginContainer">
                <div className="textLogin">
                    <h2>To view more comments</h2>
                    <h3>Sign up with</h3>
                    <div className="list">
                        <div className="ui inverted red button ">
                            <i className="facebook icon"></i>
                            Sign Up With Facebook</div>
                        <br/>
                        <div className="ui inverted red button">
                            <i className="twitter icon"></i>
                            Sign Up With Twitter</div>
                        <br/>
                        <div className="ui inverted red button">
                            <i className="google plus icon"></i>
                            Sign Up With Google</div>
                    </div>
                </div>

            </div>
        );
    }
});

var ReplyContainer = React.createClass({


    addReply: function () {

        /*todo ajax call addReaply*/

        this.props.handleReply()
    },

    render: function () {

        var bgColor = {
            color: this.props.color
        };

        return (
            <div >
                <div className="field" style={bgColor}>
                    <textarea rows="2">
                    </textarea>
                </div>
                <div className="action">
                    <a className="reply" style={bgColor} onClick={this.addReply}>
                        <i className="icon edit" ></i>
                        Add Reply</a>

                </div>
            </div>
        );
    }
});

var LoginModal = React.createClass({

    render: function () {
        return(

                <div className="ui small modal">
                    <div className="header">By continuing, I agree that I'm atleast 13 years old have read and agree to the terms of service.  </div>
                    <div className="content">
                        <div>
                            <a href="http://facebook.com" className="ui small image">
                                <img  src="../images/facebookLogin.PNG"/>
                            </a>

                                or
                            <span>
                                <a href="http://google.com" className="ui small image">
                                    <img  src="../images/sign-in-google.png"/>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div className="content">
                        <p>
                            Why do I need to sign in
                            <br/>
                            warcircle is an online platform for debate
                        </p>
                    </div>
                </div>
        );
    },
    componentDidMount: function() {
        this.node = this.getDOMNode();
        this.$modal = $(this.node);

        this.$icon = $("<i class='close icon'></i>");
        this.$header = $("<div class='header'></div>").html(this.props.header);
        this.$content = $("<div class='content'></div>");

        this.$modal.append(this.$icon);
   /*     this.$modal.append(this.$header);
        this.$modal.append(this.$content);*/

        //this.renderDialogContent(this.props);
    },

    componentWillReceiveProps: function(newProps) {
        this.renderDialogContent(newProps);
    },

    renderDialogContent: function(props) {
        props = props || this.props;

        React.render(<div>{props.children}</div>, this.$content[0]);

        if (props.open) {
            this.$modal.modal('show');
        }
        else {
            this.$modal.modal('hide modal');
        }
    },

    componentWillUnmount: function() {
        this.$modal.modal('destroy').remove();
        React.unmountComponentAtNode(this.node);
    },

    getDefaultProps: function() {
        return {
            title: ''
        }
    }
});



/*next transition */

var innerSectionLeft = React.createClass({
    render: function () {

    }

});

var innerSectionRight = React.createClass({
    render: function () {

    }

});