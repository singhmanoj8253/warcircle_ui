$(document).ready(function() {
    var updateClock = function() {
        function pad(n) {
            return (n < 10) ? '0' + n : n;
        }

        var now = new Date();
        var s = pad(now.getHours()) + ':' +
            pad(now.getMinutes()) + ':' +
            pad(now.getSeconds());

        $('#time').html(s);

        var delay = 1000 - (now % 1000);
        setTimeout(updateClock, delay);
    };
    updateClock();
    var headStyle = {
        width:'30px',
        height:'30px'

    };

    $(function() {
        // This will be called when the DOM is ready and everything has been loaded.
        $('#showAll').click(function(){  //Adds an event to
            $('#tags').toggle('slow');
        });
        $('.ui .accordion').accordion();

        $(".myButton").click(function () {

            // Set the effect type
            var effect = 'slide';

            // Set the options for the effect type chosen
            var options = { direction: "down"};

            // Set the duration (default: 400 milliseconds)
            var duration = 200;

            $('#myDiv').toggle(effect, options, duration);
        });

        $('.ui.accordion')
            .accordion()
        ;
        $('.ui.dropdown')
            .dropdown()
        ;

       /* $('.small.modal')
            .modal('show')
        ;
*/


    });
    (function ( $ ) {
        $.fn.progress = function() {
            var percent = this.data("percent");
            this.css("width", percent+"%");
        };
    }( jQuery ));

    $(document).ready(function(){
        $(".bar-one .bar").progress();
        $(".bar-two .bar").progress();
        $(".bar-three .bar").progress();
    });

    React.render(<Header />, document.getElementById('header'));
    React.render(<Page />, document.getElementById('container'));
/*    React.render(<WarDetailAccordion/>,document.getElementById('warAccordion'));
    React.render(<CommentBox color={"#28A9E0"}/>,document.getElementById('commentBox1'));
    React.render(<CommentBox color={"#F15723"}/>,document.getElementById('commentBox2'));
    React.render(<CommentedBox color={"#28A9E0"}/>,document.getElementById('commentedBox1'));
    React.render(<CommentedBox color={"#F15723"}/>,document.getElementById('commentedBox2'));
    React.render(<CommentedBox color={"#28A9E0"}/>,document.getElementById('commentedBox11'));
    React.render(<CommentedBox color={"#F15723"}/>,document.getElementById('commentedBox22'));
    React.render(<CommentedBox color={"#28A9E0"}/>,document.getElementById('commentedBox111'));
    React.render(<CommentedBox color={"#F15723"}/>,document.getElementById('commentedBox222'));
    React.render(<LoginDiv/>,document.getElementById('loginContainer'));*/

});